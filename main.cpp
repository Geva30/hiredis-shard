#include "hiredis.h"
#include <stdlib.h>
#include <string>
#include <list>
#include <map>
#include <fstream>
#include <stdio.h>
#include <iostream>

class IRedis
{
  public:
    
    virtual ~IRedis(){}
    virtual bool get(const std::string& key, std::string& value) = 0;
    virtual bool set(const std::string& key, const std::string& value) = 0;
};

typedef int (*hash)(const std::string& key, int nInstanceCount);

class SingleRedis : public IRedis
{
    public:
    
        SingleRedis() : m_pContext(NULL)
        {
        }
    
        virtual ~SingleRedis()
        {     
            disconnect();
        }
    
        virtual bool connect(const std::string& ip, int nPort)
        {
            m_pContext = redisConnect(ip.c_str(), nPort);
            if (m_pContext != NULL && m_pContext->err) {
                printf("Error: %s\n", m_pContext->errstr);
                return false;
            }
            
            printf("connected to : %s:%d\n", ip.c_str(), nPort);
            
            return true;
        }
    
        virtual void disconnect()
        {
          redisFree(m_pContext);
        }
    
        virtual bool get(const std::string& key, std::string& value)
        {
            char command[128];
            sprintf(command, "GET %s", key.c_str());
            
            redisReply *reply = (redisReply*)redisCommand(m_pContext , command);

            if(reply->type != REDIS_REPLY_STRING)
            {
                printf("'%s' failed\n", command);
                freeReplyObject(reply);
                return false;
            }
            
            value = reply->str;

            //printf("'%s' is %s\n" , key.c_str(), value.c_str());
            
            return true;
        }
    
        virtual bool set(const std::string& key, const std::string& value)
        {
            char command[128];
            sprintf(command, "SET %s %s", key.c_str(), value.c_str());
            
            redisReply *reply;
            reply = (redisReply*)redisCommand(m_pContext, command);

            if(reply->type != REDIS_REPLY_STATUS)
            {
                printf("'%s' failed\n", command);
                freeReplyObject(reply);
                return false;
            }

            freeReplyObject(reply);
            
            return true;
        }
    
    private:
        
        redisContext *m_pContext;
        
};

struct RedisAddress
{
    std::string     m_Ip;
    long            m_nPort;    
};

class MultiRedis : public IRedis
{
    public:
    
        MultiRedis(const std::string& filename, hash pHash) : m_pHash(pHash)
        {
            LoadFile(filename);
            connect();
        }
    
        virtual ~MultiRedis()
        {
            disconnect();            
        }
    
        virtual bool connect()
        {
            auto iter = m_Addresses.begin();
                
            int nInstance = 0;
            
            while (iter != m_Addresses.end())
            {
                SingleRedis* pSingleRedis = new SingleRedis();
                if (pSingleRedis->connect((*iter).m_Ip, (*iter).m_nPort) == false)
                {
                    printf("connect(%s:%d) failed\n", (*iter).m_Ip.c_str(), (*iter).m_nPort);
                    return false;   
                }
                
                m_Instances[nInstance++] = pSingleRedis;
                iter++;
            }
            
            return true;
        }
                     
        virtual void disconnect()
        {
            auto iter = m_Instances.begin();
                
            while (iter != m_Instances.end())
            {
             
                iter++;
            }
        }
    
        virtual bool get(const std::string& key, std::string& value)
        {                      
            return m_Instances[m_pHash(key, m_Addresses.size())]->get(key, value);
        }
    
        virtual bool set(const std::string& key, const std::string& value)
        {
            return m_Instances[m_pHash(key, m_Addresses.size())]->set(key, value);
        }
                     
    private:
        
        bool LoadFile(const std::string& filename)
        {
            std::string line ;
            std::ifstream infile(filename.c_str());
            if ( infile ) 
            {
                while ( getline( infile , line ) != 0 ) 
                {
                    RedisAddress redisAddress;
                    char buffer[32];
                    sscanf(line.c_str(), "%s %d", &buffer, &redisAddress.m_nPort);
                    redisAddress.m_Ip = buffer;
                    //printf("address : %s:%d\n", redisAddress.m_Ip.c_str(), redisAddress.m_nPort);
                    m_Addresses.push_back(redisAddress);
                }
            }
            else
            {
                printf("cannot read file %s\n", filename.c_str());
                exit(-1);   
            }
            
            infile.close();
                           
            return true;
        }                     
    
    private:
        
        hash                            m_pHash;
        std::map<int, SingleRedis*>     m_Instances;        
        std::list<RedisAddress>         m_Addresses;
};
       
class Redis : public IRedis
{
    public:
    
        Redis(const std::string& Ip, int nPort)
        {
            SingleRedis* pRedis = new SingleRedis();
            if (pRedis->connect(Ip, nPort) == false)
            {
                printf("failed to connect to %s:%d\n", Ip.c_str(), nPort);
            }
            
            m_pImpl = pRedis;
        }
    
        Redis(const std::string& filename, hash pHash)
        {
            MultiRedis *pRedis = new MultiRedis(filename, pHash);
            m_pImpl = pRedis;          
        }

        virtual ~Redis()
        {
            delete m_pImpl;
            m_pImpl = 0;
        }

        virtual bool get(const std::string& key, std::string& value)
        {
            return m_pImpl->get(key, value);
        }

        virtual bool set(const std::string& key, const std::string& value)
        {
            return m_pImpl->set(key, value);
        }

    private:
    
        IRedis* m_pImpl;
    
};

int simple_hash(const std::string& key, int nInstanceCount)
{
    if (key.size() == 0 )
    {
        return 0;   
    }
    
    return (int)key[0] % (nInstanceCount);
}

void run_tests(IRedis* pRedis, int iterations, const std::string& message)
{
    char key[10];
    char value[] = {"skfdjthfdsjfgdfghuv"};
    std::string dummyValue;
    struct timeval t0, t1;
    
    gettimeofday(&t0, 0);
    
    for (int i = 0 ; i < iterations ; i++)
    {
        sprintf(key, "%d", rand() % 100000);       
        pRedis->set(key, value);
        pRedis->get(key, dummyValue);
    }
    
    
    gettimeofday(&t1, 0);
    long elapsed = (t1.tv_sec-t0.tv_sec);
    
    printf("(%s) duration: %d sec\n", message.c_str(), elapsed);
}

int main(int argc, char** argv)
{
    printf("running...\n");
    
    // single redis instance
    Redis singleRedis("127.0.0.1", 6379);
    run_tests(&singleRedis, 100000, "single");
    
    // multi redis instance
    Redis multiRedis("redis.conf", simple_hash);
    run_tests(&multiRedis, 100000, "multi");
    
    return 0;
}
